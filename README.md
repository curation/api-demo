### Curation API demo 
Simple demo of logging in and executing a very simple express API proxy over the Curation GraphQL API. In production, it is recommended to use [Apollo Link](https://github.com/apollographql/apollo-link) for communication. 

Run `npm install`, then `node index.js` with `EMAIL_ADDRESS` and `PASSWORD` environment variables. 