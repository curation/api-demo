const { get, post } = require("axios");
const express = require("express");
const bodyparser = require("body-parser");
const app = new express();
app.use(bodyparser.json({ extended: true }));
const email = process.env.EMAIL_ADDRESS;
const password = process.env.PASSWORD;
const BASE_URL = process.env.BASE_URL || "https://app.curationcorp.com/api";

// Test commit

if (!email || !password){
  console.error('need username and password')
  process.exit(1)
}

let ApiKey = ''; 
const getApiKey = async () => {
  try {
    const login = await post(`${BASE_URL}/login`, {
      email,
      password
    });
    if (login.data && login.status === 200) {
      console.log(login.data)
      ApiKey = login.data.ApiKey;
    }
  } catch (e) {
    console.error(`login failed: ${e}`);
  }
};

getApiKey();

app.get("/stories", async (req, res) => {
  try {
    console.log(`Bearer ${ApiKey}`)
    const { data } = await post(
      `${BASE_URL}/graphql`,
      {
        query: `{
        themes {
          Name 
          Articles { 
            Title 
            Body
          }
         }
       }`
      },
      { headers: { Authorization: `Bearer ${ApiKey}` } }
    );
    res.send({ stories: data });
  } catch (e) {
    res.status(500).json(e.message);
  }
});

app.listen(3002, () => "app booted");
